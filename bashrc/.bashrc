# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

#test
#PS1='\w\$ '

# don't put duplicate lines in the history. See bash(1) for more options
# don't overwrite GNU Midnight Commander's setting of `ignorespace'.
HISTCONTROL=$HISTCONTROL${HISTCONTROL+:}ignoredups
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
#export HISTSIZE=10000		# save 10000 items in history

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

PAGER=/usr/bin/less
#LESS='-i -e -M -P%t?f%f :stdin .?pb%pb\%:?lbLine %lb:?bbByte %bb:-...'
#where did my 'history' go?
#HISTFILE=~/.history/history.$HOSTNAME
#export HISTFILE LESS
export PAGER

#from xprofile in .Machfiles
# Environment variables set everywhere
export EDITOR="nvim"
export TERMINAL="st"
#export BROWSER="w3m"
#bod orig
export VISUAL=nvim
export BROWSER="/opt/firefox67/firefox"
export VIMCONFIG=~/.config/nvim
export MYVIMRC=~/scripts/neovim-config/init.vim
export VIMDATA=~/.local/share/nvim

# For QT Themes
#export QT_QPA_PLATFORMTHEME=qt5ct

# XDG Paths
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share

# Compositor
picom -b

# Hide mouse when typing
#xbanish &

# zsh config dir
export ZDOTDIR=$HOME/.config/zsh
#source ~/.config/zsh/.zshrc

# Screenshots
#flameshot &

# Bluetooth systray icon
#blueman-applet &

# helps with puls audio
#pasystray &

# Easy file sharing
#pcloudcc -u chris.machine@pm.me -m ~/Cloud -d

# Screenkey applet (this will break everything)
# screenkey --start-disabled

# remap caps to escape c@m
#setxkbmap -option caps:escape
#bod original
setxkbmap -layout gb -option ctrl:nocaps
setxkbmap -option ctrl:menu_rctrl
# swap escape and caps
# setxkbmap -option caps:swapescape

# load Xresources
xrdb ~/.Xresources

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
#if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
#    debian_chroot=$(cat /etc/debian_chroot)
#fi

#    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
#else
#    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
#fi

# If this is an xterm set the title to user@host:dir
#case "$TERM" in
#xterm*|rxvt*)
#    PS1="\[\e]0;${debian_chroot:+($debian_chroot)} \w\a\]$PS1"
#    ;;
#*)
#    ;;
#esac


# some alias definitions in ~/.bash_aliases

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [ -f ~/.alias]; then
    . ~/.alias
fi
# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
#if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
#    . /etc/bash_completion
#fi

# Enable vi mode
#set -o vi


# Reload .bashrc
alias refresh='. ~/.bashrc'

#using ranger as file manager
function ranger-cd {
  ranger --choosedir=/tmp/chosen
  if [ -f /tmp/chosen -a "$(cat /tmp/chosen)" != "$(pwd | tr -d "\n")" ]; then
    cd "$(cat /tmp/chosen)"
  fi
  rm -f /tmp/chosen
}
#interfers with tmux
#bind '"\C-o":"ranger-cd\C-m"'

#add sbin to $PATH- is this syntax correct and is it wise to do so?
#export PATH=$PATH:/sbin:/usr/sbin

 PATH=$PATH:/usr/sbin/pm-suspend
export PATH

#to enable weechat core files even before it starts (whatever that means)
ulimit -c 200000

# Sets the Mail Environment Variable
#MAIL=/var/spool/mail/briain && export MAIL

#[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# added by Anaconda3 5.3.1 installer
# >>> conda init >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$(CONDA_REPORT_ERRORS=false '/home/briain/scripts/python/anaconda3/bin/conda' shell.bash hook 2> /dev/null)"
if [ $? -eq 0 ]; then
    \eval "$__conda_setup"
else
    if [ -f "/home/briain/scripts/python/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/home/briain/scripts/python/anaconda3/etc/profile.d/conda.sh"
        CONDA_CHANGEPS1=false conda activate base
    else
        \export PATH="/home/briain/scripts/python/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda init <<<

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
export PATH=~/.npm-global/bin:$PATH
. "$HOME/.cargo/env"
