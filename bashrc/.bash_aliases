alias vim="nvim"
alias nv="nvim"
alias nvi="nvim ~/scripts/neovim-config/init.vim"
alias v="nvim"
alias lg="lynx www.google.ie"
alias mr="sudo /etc/init.d/mpd stop && /etc/init.d/mpd start"
alias nc="setxkbmap -option ctrl:nocaps"
alias ma="sudo mount -a"
alias io="sudo ifdown eth0 && sudo ifup eth0"
alias td="fusermount -u /media/tipo"
alias lsd="find . -type d -print | sort | more"
alias weechat="weechat-curses"
alias srng="sudo ranger"
alias rng="ranger"
alias r="ranger"
alias briss="/usr/bin/java -jar /home/briain/scripts/software/briss-0.9/briss-0.9.jar"
alias ..='cd ..'
alias ll='ls -l | less'
alias la='ls -a'
alias l='ls -CF'
alias c="clear"
alias cr="rm ~/.local/share/rhythmbox/rhythmdb.xml"
alias cl="clear; ls -CF"
alias m="less"
alias j="jobs"
alias pi="ps aux | grep"
alias rr="rm -R"
alias k="kill"
alias dot='ls .[a-zA-Z0-9_]*'
alias apt="sudo aptitude"
alias zz="sudo /usr/sbin/pm-suspend"
#from elsewhere
alias ss="ps -aux"
alias dir='ls -ba'
alias tree='tree -C'
alias trls='tree -C | less -R'	# -C outputs colour, -R makes less understand color
alias mode='(set -o | grep emacs.*on >/dev/null 2>&1 && echo "emacs mode" || echo "vi mode")'

# enable color support of ls and also add handy aliases
#if [ -x /usr/bin/dircolors ]; then
#    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
#    alias ls='ls --color=auto'
#    alias dir='dir --color=auto'
#    alias vdir='vdir --color=auto'
#    alias grep='grep --color=auto'
#    alias fgrep='fgrep --color=auto'
#    alias egrep='egrep --color=auto'
#fi
